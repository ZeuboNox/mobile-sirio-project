package com.example.payuapp.API;

/**
 * interface utilisé pour récupérer une information via une execution asynchrone.
 * @param <T>
 */
public interface HandlerAPI<T> {
    public void Awake(T obj);
}
