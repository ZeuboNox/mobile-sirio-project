package com.example.payuapp.API;

import com.example.payuapp.Article.Article;
import com.example.payuapp.Article.Categorie;
import com.example.payuapp.commande.Commande;
import com.example.payuapp.connexion.User;

import org.bson.types.ObjectId;

import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * interface pour créé une requete retrofit
 */
public interface ModelAPI {

    @GET("none/")
    public default Call<Object> getEmpty() {
        return null;
    }

    @GET(" ")
    public default Call<List<Article>> getArticle() {
        return null;
    }

    @GET("categories")
    public default Call<List<Categorie>> getCategories() {
        return null;
    }

    @GET("connexion/")
    public default Call<User> getUser(@Query("email") String user, @Query("password") String pass) {
        return null;
    }
    @GET("utilisateurs/{id}/commandes")
    public default Call<List<Commande>> getCommand(@Path("id") ObjectId id) {
        return null;
    }


    @Headers({"Content-Type: application/json"})
    @POST("utilisateurs/{id}/commandes")
    public default Call<Commande> postCommand(@Path("id") ObjectId id, @Body RequestBody body) {
        return null;
    }

    @Headers({"Content-Type: application/json"})
    @PUT("utilisateurs/{id}")
    public default Call<ResponseBody> updateUser(@Path("id") ObjectId id, @Body User body){
        return null;
    }

    @POST("utilisateurs")
    public default Call<ResponseBody> postUtilisateur(@Body RequestBody body){
        return null;
    }

}
