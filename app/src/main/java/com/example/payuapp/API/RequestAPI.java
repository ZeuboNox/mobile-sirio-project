package com.example.payuapp.API;


import android.app.ProgressDialog;
import android.util.Log;

import com.example.payuapp.Parameter;
import com.example.payuapp.Singleton.Singleton;
import com.example.payuapp.Singleton.SingletonWaiting;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Classe qui a pour role d'executer les requete API
 * T correspond au type du retour, selon la requete.
 * en cas de GET le T seras la classe correspondant au json
 * en cas de POST ou PUT le T seras un ResponseBody
 * @param <T>
 */
public class RequestAPI<T> implements Callback<T> {

    private static final String BASE_URL = "http://3.142.142.32:9000/";
    HandlerAPI<T> handler;
    final SingletonWaiting waiter = (SingletonWaiting) Singleton.getInstance(SingletonWaiting.class);

    String typeOf;
    Parameter params;

    /**
     * methode principal pour executé une requete API
     * typeOf :
     * - List<Article>      requete GET
     * - UpdateUser         requete PUT
     * - User               requete GET
     * - List<Commande>     requete GET
     * - List<Categorie>    requete GET
     * - PostUser           requete POST
     * - Commande           requete POST
     *
     * @param hndl: HandlerAPI, classe a l'origine de l'appel de cette fonction, une classe qui est implementé avec un HandlerAPI
     * @param typeOf: String, nom de l'execution a effectué sur l'API
     * @param params: Parameter, information necessaire au bon fonctionnement de la requete
     */
    public void Api(HandlerAPI<T> hndl, String typeOf, Parameter params){
        this.typeOf = typeOf;
        this.params = params;
        handler = hndl;
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ModelAPI apiService = retrofit.create(ModelAPI.class);

        Call<T> call;
        switch(typeOf){
            case "List<Article>":
                call = (Call<T>) apiService.getArticle();
                break;
            case "UpdateUser":
                call = (Call<T>) apiService.updateUser(params.objUser.getId(), params.objUser);
                break;
            case "User":
                call = (Call<T>) apiService.getUser(params.user, params.pass);
                break;
            case "List<Commande>":
                call = (Call<T>) apiService.getCommand(params.idUser);
                break;
            case "List<Categorie>":
                call = (Call<T>) apiService.getCategories();
                break;
            case "PostUser":
                call = (Call<T>) apiService.postUtilisateur(params.request);
                break;
            case "Commande":
                call = (Call<T>) apiService.postCommand(params.objUser.getId(), params.request);
                break;

            default:
                call = null;
                Log.d("ERRORING", "pas de call possible, typeOf = "+typeOf);
                return;
        }
        call.enqueue(this);
    }

    @Override
    /**
     * En cas de reussite fais appel a la methode awake de la classe implementé du HandlerAPI
     */
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful()) {
            Log.i("INFORMATION", " maked "+ typeOf +" request with " +  call.request() + " " + response.message());
            T obj = response.body();
            handler.Awake(obj);
        } else {

            if (waiter.progressDialog.isShowing())
                waiter.progressDialog.dismiss();
            Log.i("INFORMATION", " probleme "+ typeOf + " value Error " + response.code() +"\nrequest with " +  call.request() + " " + response.message());
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        if (waiter.progressDialog.isShowing())
            waiter.progressDialog.dismiss();
        Log.i("INFORMATION", " Couldn't make object for "+ typeOf + " \n\n " + t.toString() +"\nfor " +  call.request());
        if(params.alwaysAwake){
            handler.Awake(null);
        }
    }
}
