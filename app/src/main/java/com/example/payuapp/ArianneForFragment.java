package com.example.payuapp;


import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.payuapp.Singleton.Singleton;

import java.lang.reflect.Type;

/**
 * classe pour gerer le fils d'arianne de l'application
 */
public class ArianneForFragment extends Singleton{

    private FragmentManager fragmentManager;
    private int containerViewId;
    private ImageButton arianneButton;

    public ArianneForFragment(FragmentManager fragMan, int containView, ImageButton arianneButton){
        fragmentManager = fragMan;
        containerViewId = containView;

        fragmentManager.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                checkButton();
                Log.d("CREATION", String.valueOf(fragmentManager.getBackStackEntryCount()));
            }
        });

        this.arianneButton = arianneButton;
        this.arianneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                backPage();
            }
        });
        Singleton.changeInstance(ArianneForFragment.class, this);
    }

    /**
     * file de demarrage
     * @param fragment :Fragment
     */
    public void startPage(Fragment fragment){
        BackButton.disable(arianneButton);

        fragmentManager.popBackStack("stack", FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction ft = fragmentManager.beginTransaction().replace(containerViewId, fragment);
        ft.commit();
    }

    /**
     * prochain fragment dans la file
     * @param fragment
     */
    public void nextPage(Fragment fragment){
        BackButton.active(arianneButton);
        FragmentTransaction ft = fragmentManager.beginTransaction().replace(containerViewId, fragment).addToBackStack("stack");
        ft.commit();
    }

    /**
     * retour au fragment precedent
     */
    public void backPage(){
        fragmentManager.popBackStack();
    }

    /**
     * affiche le boutton precedent si il existe plus de 1 fragment dans la file
     */
    public void checkButton(){
        if(fragmentManager.getBackStackEntryCount() < 1){
            BackButton.disable(arianneButton);
        }
    }


    @Override
    public Object toSave() {
        return null;
    }

    @Override
    public void reset() {

    }

    @Override
    public Type type() {
        return null;
    }

    @Override
    public void toLoad(Object obj) {

    }
}
