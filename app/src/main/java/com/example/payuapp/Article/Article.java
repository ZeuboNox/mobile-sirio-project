package com.example.payuapp.Article;

import android.util.Log;

import com.example.payuapp.Search.TreeSearch;
import com.example.payuapp.commande.Cart;
import com.google.gson.annotations.SerializedName;

import org.bson.types.ObjectId;

import java.util.Date;
import java.util.List;

/**
 * classe correspondant a un article JSON du projet
 */
public class Article {
    @SerializedName("categorie")
    private String categorie = "";

    @SerializedName("createdAt")
    private Date createdAt = null;

    @SerializedName("updatedAt")
    private Date updatedAt = null;

    @SerializedName("id")
    private ObjectId idProduit = null;

    @SerializedName("title")
    private String libelle = "Empty";

    @SerializedName("description")
    private String description = "Empty";

    @SerializedName("price")
    private String prix = "Empty";

    @SerializedName("inStock")
    private int nbStock = 0;

    @SerializedName("images")
    private List<Image> images = null;

    private TreeSearch treeTag;
    private int priceWeight;

    public boolean inPanier = false;
    public boolean inFavoris = false;

    public String getCategorie() {
        return categorie;
    }

    public int getIdProduit() {
        try {
            return (int) idProduit.getDate().getTime();
        } catch(Exception e){
            return -1;
        }
    }
    public ObjectId getId() {
        return idProduit;
    }

    public String getLibelle() {
        return libelle;
    }

    public String getDescription() {
        return description;
    }

    public String getPrix() {
        return prix;
    }

    public int getNbStock() {
        return nbStock;
    }

    public TreeSearch getTreeTag() {
        return treeTag;
    }

    public int getPriceWeight() {
        return priceWeight;
    }

    public void setPriceWeight(int priceWeight) {
        this.priceWeight = priceWeight;
    }

    /**
     * methode qui genere les mots pour effectué une recherche
     */
    public void makeTag() {
        treeTag = new TreeSearch();
        if(libelle == null){
            return;
        }
        String[] part = categorie.split(" ");
        for (int i = 0; i < part.length; i++) {
            Log.d("DEBUGAGE", part[i]);
            treeTag.addWord(part[i]);
        }
        part = libelle.split(" ");
        for (int i = 0; i < part.length; i++) {
            Log.d("DEBUGAGE", part[i]);
            treeTag.addWord(part[i]);
        }
    }

    /**
     * methode qui permet de convertir un Article en Cart
     * @return :Cart
     */
    public Cart toCart(){
        Cart cart = new Cart();
        cart.setId(idProduit);
        cart.setImages(images);
        cart.setInStock(nbStock);
        cart.setPrice(Float.parseFloat(prix));
        cart.setTitle(libelle);
        return  cart;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }
}
