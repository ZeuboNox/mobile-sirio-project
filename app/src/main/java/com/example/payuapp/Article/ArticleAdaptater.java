package com.example.payuapp.Article;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.payuapp.R;
import com.zozx.glider.Glider;

import java.util.List;

/**
 * Adapter pour afficher la liste des articles dans la gridview du fragment HomeFragment
 */
public class ArticleAdaptater extends BaseAdapter {

    Context context;
    List<Article> articles;
    LayoutInflater inflater;

    public ArticleAdaptater(Context context, List<Article> articles){
        this.context = context;
        this.articles = articles;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return articles.size();
    }

    @Override
    public Object getItem(int position) {
        return articles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.gridview_layout, null); // inflate the layout
        TextView title = convertView.findViewById(R.id.titleArticle);
        TextView price = convertView.findViewById(R.id.priceArticle);
        ImageView img = convertView.findViewById(R.id.imageArticle);
        Glider.load(img, articles.get(i).getImages().get(0).getUrl());
        String lab = articles.get(i).getLibelle();
        title.setText(lab.length()>=47?lab.substring(0,47)+"...":lab);
        price.setText(articles.get(i).getPrix().toString() + "€");
        return convertView;
    }
}
