package com.example.payuapp.Article;

import com.google.gson.annotations.SerializedName;

import org.bson.types.ObjectId;

import java.util.Date;

/**
 * classe correspondant a une categorie JSON du projet
 */
public class Categorie {
    @SerializedName("id")
    private ObjectId id = null;

    @SerializedName("name")
    private String libelle = "";

    @SerializedName("createdAt")
    private Date createdAt = null;

    @SerializedName("updatedAt")
    private Date updatedAt = null;


    public ObjectId getId() {
        return id;
    }

    public String getLibelle() {
        return libelle;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
