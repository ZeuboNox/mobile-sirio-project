package com.example.payuapp.Article;

import com.google.gson.annotations.SerializedName;

/**
 * classe correspondant a une Image JSON du projet
 */
public class Image {
    @SerializedName("public_id")
    private String public_id = "";

    @SerializedName("url")
    private String url = "";

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
