package com.example.payuapp.Article;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.zozx.glider.Glider;

import java.util.List;

/**
 * Adapter pour afficher la liste des Image dans la ViewPager du fragment ArticleFragment
 */
public class ImageAdapter extends PagerAdapter {

    private Context context;
    private List<Image> images;

    public ImageAdapter(Context context, List<Image> images){
        this.context = context;
        this.images = images;

    }

    @Override
    public int getCount() {
        return images.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView img = new ImageView(context);
        img.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Glider.load(img, images.get(position).getUrl());
        container.addView(img,0);
        return img;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((ImageView)object);
    }
}
