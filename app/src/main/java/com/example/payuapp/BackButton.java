package com.example.payuapp;

import android.app.ActionBar;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * classe pour adapter le visuel d'un boutton (pas optimal)
 */
public class BackButton{

    public static void active(ImageButton imgButton){
        imgButton.setLayoutParams(
                new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.MATCH_PARENT));
    }
    public static void active(Button imgButton){
        imgButton.setLayoutParams(
                new LinearLayout.LayoutParams(
                        300,
                        LinearLayout.LayoutParams.MATCH_PARENT));
    }
    public static void active(EditText text){
        text.setLayoutParams(
                new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT,
                        FrameLayout.LayoutParams.WRAP_CONTENT));
    }
    public static void active(TextView text){
        text.setLayoutParams(
                new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT,
                        FrameLayout.LayoutParams.WRAP_CONTENT));
    }
    public static void disable(EditText text){
        text.setLayoutParams(
                new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT,
                        0));
    }
    public static void disable(TextView text){
        text.setLayoutParams(
                new FrameLayout.LayoutParams(
                        FrameLayout.LayoutParams.MATCH_PARENT,
                        0));
    }

    public static void disable(ImageButton imgButton){
        imgButton.setLayoutParams(
                new LinearLayout.LayoutParams(
                        0,
                        LinearLayout.LayoutParams.MATCH_PARENT));
    }
    public static void disable(Button imgButton){
        imgButton.setLayoutParams(
                new LinearLayout.LayoutParams(
                        0,
                        LinearLayout.LayoutParams.MATCH_PARENT));
    }

}
