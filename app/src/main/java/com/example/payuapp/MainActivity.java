package com.example.payuapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;

import com.example.payuapp.API.RequestAPI;
import com.example.payuapp.API.HandlerAPI;
import com.example.payuapp.Article.Categorie;
import com.example.payuapp.Singleton.Singleton;
import com.example.payuapp.Singleton.SingletonArticles;
import com.example.payuapp.Singleton.SingletonFavoris;
import com.example.payuapp.databinding.ActivityMainBinding;
import com.example.payuapp.fragment.FavorisFragment;
import com.example.payuapp.fragment.HomeFragment;
import com.example.payuapp.fragment.PanierFragment;
import com.example.payuapp.fragment.PortailFragment;
import com.example.payuapp.fragment.SearchFragment;
import com.example.payuapp.panier.Panier;
import com.google.android.material.badge.BadgeDrawable;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.List;


public class MainActivity extends AppCompatActivity implements HandlerAPI<List<Categorie>> {

    ArianneForFragment arianne;
    ActivityMainBinding binding;

    final Panier panier = (Panier) Singleton.getInstance(Panier.class);
    final SingletonArticles singleArt = (SingletonArticles) Singleton.getInstance(SingletonArticles.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        arianne = new ArianneForFragment(getSupportFragmentManager(), R.id.fragment_layout, findViewById(R.id.imgButton));

        BottomNavigationView navigationBottom = findViewById(R.id.bottomNavigationView);
        navigationBottom.setOnNavigationItemSelectedListener(navListener());

        arianne.startPage(new HomeFragment());

        /*getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_layout, new HomeFragment())
                .commit();*/

        BadgeDrawable badge=binding.bottomNavigationView.getOrCreateBadge(R.id.shop);
        badge.setBackgroundColor(Color.BLUE);
        badge.setBackgroundColor(ContextCompat.getColor(getBaseContext(), R.color.purple_500));
        badge.setMaxCharacterCount(3);
        badge.setVisible(false);

        RequestAPI<List<Categorie>> apiCategorie = new RequestAPI<>();
        apiCategorie.Api(this, "List<Categorie>", new Parameter());

        panier.setBadge(badge);
        Singleton.load(getBaseContext(), SingletonFavoris.class, true);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener navListener(){
        return new BottomNavigationView.OnNavigationItemSelectedListener(){
            Fragment selectFragment = null;
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.home2:
                        selectFragment = new HomeFragment();
                        break;
                    case R.id.connexion:
                        selectFragment = new PortailFragment();
                        break;
                    case R.id.search:
                        selectFragment = new SearchFragment();
                        break;
                    case R.id.shop:
                        selectFragment = new PanierFragment();
                        break;
                    case R.id.favorite:
                        selectFragment = new FavorisFragment();
                        break;
                }

                if(selectFragment!=null)
                    arianne.startPage(selectFragment);

                /*getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_layout, selectFragment)
                        .commit();*/
                return true;
            }
        };
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding = null;
    }

    @Override
    public void Awake(List<Categorie> obj) {
        singleArt.categories = obj;
    }
}