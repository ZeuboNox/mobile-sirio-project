package com.example.payuapp;

import com.example.payuapp.commande.Commande;
import com.example.payuapp.connexion.User;

import org.bson.types.ObjectId;

import okhttp3.RequestBody;

/**
 * classe pour effectuer un passage d'information dynamique dans une methode
 */
public class Parameter {
    public String user = "";
    public String pass = "";
    public User objUser = null;
    public RequestBody request = null;
    public ObjectId idUser = null;
    public boolean needAwake = false;
    public boolean alwaysAwake = false;
    public Commande objCommand = new Commande();
}
