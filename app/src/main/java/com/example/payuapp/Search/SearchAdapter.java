package com.example.payuapp.Search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.payuapp.Article.Article;
import com.example.payuapp.R;

import java.util.ArrayList;

/**
 * Adapter pour afficher la liste des mots trouvé dans la listView du fragment SearchFragment
 */
public class SearchAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> find;
    LayoutInflater inflater;

    public SearchAdapter(Context context, ArrayList<String> find){
        this.context = context;
        this.find = find;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return find.size();
    }

    @Override
    public Object getItem(int position) {
        return find.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.search_layout, null); // inflate the layout
        TextView findView = convertView.findViewById(R.id.findTextLayout);
        findView.setText(find.get(i));
        return convertView;
    }
}
