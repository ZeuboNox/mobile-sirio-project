package com.example.payuapp.Search;

import android.util.Log;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Dictionary;

/**
 * class qui permet de cree une arboraissance de lettre
 */
public class TreeSearch {
    private Noeud start;
    private ArrayList<String> wordsList;

    public TreeSearch(){
        start = new Noeud();
        wordsList = new ArrayList();

    }

    public int indexChar(char character){
        return character - 'a';
    }

    public char ConvertIntToLetter(int number){
        return (char) (number + 'a');
    }

    /**
     * methode qui permet d'ajouter un mot dans l'arborescence
     * @param word :String, mot a rajouter
     */
    public void addWord(String word){
        word = word.toLowerCase();
        word = stripAccents(word);
        nextLetter(word,0,start);
    }

    /**
     * methode qui permet d'ajouter une liste de mot dans l'arborescence
     * @param words :ArrayList<String>, mots a rajouter
     */
    public void addWord(ArrayList<String> words){
        int len = words.size();
        for(int i =0; i<len ;i++){
            nextLetter(stripAccents(words.get(i).toLowerCase()),0,start);
        }

    }

    /**
     * methode récursive pour ajouter les mots
     * @param word :String, mot complet
     * @param i :int, emplacement de la lettre par rapport au mot
     * @param actualNode :Noeud, noeud corresondant a la lettre
     */
    private void nextLetter(String word, int i, Noeud actualNode){

        if(word.length() == i){
            actualNode.isEnd = true;
            return;
        }
        int valChar = Character.getNumericValue(word.charAt(i));
        int pos = valChar>=10 ? indexChar(word.charAt(i)):valChar+27;
        if(actualNode.branch[pos] == null){
            actualNode.branch[pos] = new Noeud();
        }

        nextLetter(word, i+1, actualNode.branch[pos]);
    }

    /**
     * recupere tous les mot sauvegarder dans l'arborescence
     * @return :ArrayList<String>
     */
    public ArrayList<String> getAllWord(){
        wordsList.clear();
        searchAll("", -1, start);
        return wordsList;
    }

    /**
     * recherche des mots dans l'arborescence
     * @param searchWord: String, le mot/sequence de lettre
     * @return :ArrayList<String>, les mot retrouvé retourné en liste
     */
    public ArrayList<String> search(String searchWord){
        wordsList.clear();
        if(searchWord.length() == 0){
            return wordsList;
        }

        searchWord = searchWord.toLowerCase();
        Noeud findAfter = findNode(searchWord, 0,start);

        if(findAfter == null){
            Log.d("DEBUGAGE", "NULL NODE");
        }

        searchAll(searchWord, -1, findAfter);
        return wordsList;
    }

    /**
     * recherche recursive d'un mot
     * @param word :String, mot a chercher
     * @param index :int, emplacement de la lettre par rapport au mot
     * @param actualNode :Noeud, noeud corresondant a la lettre
     * @return :Noeud
     */
    private Noeud findNode(String word, int index, Noeud actualNode){
        if(actualNode == null){
            return null;
        }

        if(word.length() == index){
            return actualNode;
        }

        int pose = indexChar(word.charAt(index));
        return findNode(word, index+1, actualNode.branch[pose]);
    }

    private void searchAll(String wordTmp, int index,Noeud actualNode){
        if(actualNode == null){
            return;
        }

        if(index!=-1)
            wordTmp += ConvertIntToLetter(index);

        if(actualNode.isEnd){
            wordsList.add(wordTmp);
        }

        for(int i =0; i<26;i++){
            searchAll(wordTmp, i,actualNode.branch[i]);
        }
    }

    /**
     * enleve les accents d'une lettre
     * @param s :String, la lettre
     * @return
     */
    public String stripAccents(String s)
    {
        s = Normalizer.normalize(s, Normalizer.Form.NFD);
        s = s.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");
        return s;
    }

    public void debugContentTree(){
        wordsList.clear();
        searchAll("",-1,start);
        Log.d("DEBUGAGE", wordsList.toString());

    }
}

/**
 * classe qui stock chaque lettre et si ce noeud est la fin d'un mot
 */
class Noeud{
    public Noeud[] branch;
    public boolean isEnd;

    public Noeud(){
        branch = new Noeud[36];
        isEnd = false;
    }


}