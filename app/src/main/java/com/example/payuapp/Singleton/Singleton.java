package com.example.payuapp.Singleton;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.example.payuapp.Article.Article;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * classe qui permet de gerer les singleton
 */
public abstract class Singleton {

    private static Map<Class, Singleton> INSTANCE = null;

    public abstract Object toSave();
    public abstract void reset();
    public abstract Type type();
    public abstract void toLoad(Object obj);

    private static void init(){
        INSTANCE = new HashMap<>();
    }

    /** Point d'accès pour l'instance unique du singleton */
    public static synchronized Singleton getInstance(Class name){
        if (INSTANCE == null)
        {
            init();
        }
        if(!INSTANCE.containsKey(name)){
            Singleton object = null;
            try {
                object = (Singleton) name.newInstance();
                Log.i("INFORMATION", "instantiate method for " + name.toString() + " make");
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                Log.i("INFORMATION", "instantiate method for " + name.toString() + " miss, access impossible");
            } catch (InstantiationException e) {
                e.printStackTrace();
                Log.i("INFORMATION", "instantiate method for " + name.toString() + " miss, couldn't instantiate");
            }
            INSTANCE.put(name, object);
        }
        return INSTANCE.get(name);
    }

    public static void changeInstance(Class name, Singleton obj){
        Singleton.getInstance(name);
        INSTANCE.replace(name, obj);
    }

    /**
     * sauvegarde une classe dans un fichier
     * @param context :Context, contexte de l'application
     * @param name :Class, la class presente dans le singleton
     */
    public static synchronized void save (Context context, Class name){
        Singleton.getInstance(name);
        if(INSTANCE.get(name).toSave() == null){
            return;
        }

        Gson gson = new Gson();
        String json = gson.toJson(INSTANCE.get(name).toSave());

        SharedPreferences sharedPreferences = context.getSharedPreferences(
                "shared"+name.toString(),
                MODE_PRIVATE
        );
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(name.toString(), json);

        Log.i("INFORMATION", "save method for " + name.toString() + "\ncommitted : " + editor.commit());
    }

    /**
     * recupere les donnée d'une classe dans un fichier
     *
     * @param context :Context, contexte de l'application
     * @param name :Class, la class presente dans le singleton
     * @param listMode :boolean, option pour recuperer une liste (true) ou une classe (false)
     */
    public static synchronized void load(Context context, Class name, boolean listMode){
        Singleton.getInstance(name);
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                "shared"+name.toString(),
                MODE_PRIVATE
        );
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        INSTANCE.get(name).reset();
        String json = sharedPreferences.getString(name.toString(), "");
        if(json != "") {
            Object tmp;
            if(listMode){
                Type type = INSTANCE.get(name).type();
                tmp = gson.fromJson(json,  type);
            }else{
                tmp = name.cast(gson.fromJson(json, INSTANCE.get(name).toSave().getClass()));
            }

            Log.i("INFORMATION", "load method for " + name.toString() + "\nresult : " + tmp);

            INSTANCE.get(name).toLoad(tmp);
        }
    }

    /**
     * supprime la sauvegarde d'une class dans le fichier
     *
     * @param context :Context, contexte de l'application
     * @param name :Class, la class presente dans le singleton
     */
    public static synchronized void deleteSave(Context context, Class name){
        Singleton.getInstance(name);
        SharedPreferences sharedPreferences = context.getSharedPreferences(
                "shared"+name.toString(),
                MODE_PRIVATE
        );
        SharedPreferences.Editor editor = sharedPreferences.edit();

        INSTANCE.get(name).reset();
        Log.i("INFORMATION", "delete method for " + name.toString() + "\ncommitted ? " + editor.clear().commit());
    }

    /*
    private Singleton()
    {}

    private static Singleton INSTANCE = null;

    public static synchronized Singleton getInstance()
    {
        if (INSTANCE == null)
        {   INSTANCE = new Singleton();
        }
        return INSTANCE;
    }*/
}
