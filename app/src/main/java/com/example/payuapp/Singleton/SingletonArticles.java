package com.example.payuapp.Singleton;

import com.example.payuapp.Article.Article;
import com.example.payuapp.Article.Categorie;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SingletonArticles extends Singleton
{
    public List<Article> articles = null;
    public Map<Integer, Article> articlesDico = null;
    public List<Categorie> categories = null;

    @Override
    public Object toSave() {
        return null;
    }

    @Override
    public void reset() {

    }

    @Override
    public Type type() {
        return null;
    }

    @Override
    public void toLoad(Object obj) {

    }

    public List<String> getStringCategorie(){
        ArrayList<String> str = new ArrayList<>();
        for (Categorie c: categories) {
            str.add(c.getLibelle());
        }
        return str;
    }
}