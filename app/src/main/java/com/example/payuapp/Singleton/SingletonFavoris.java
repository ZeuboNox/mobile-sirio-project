package com.example.payuapp.Singleton;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.example.payuapp.Article.Article;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class SingletonFavoris extends Singleton{
    public List<Article> articlesFavoris;


    @Override
    public List<Article> toSave() {
        return articlesFavoris;
    }

    @Override
    public void reset() {
        articlesFavoris = new ArrayList<>();
    }

    @Override
    public Type type() {
        return new TypeToken<ArrayList<Article>>(){}.getType();
    }

    @Override
    public void toLoad(Object obj) {
        articlesFavoris = (List<Article>) obj;
    }
}
