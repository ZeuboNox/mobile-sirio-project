package com.example.payuapp.Singleton;

import com.example.payuapp.Article.Article;
import com.example.payuapp.Search.TreeSearch;

import java.lang.reflect.Type;
import java.util.List;

public class SingletonTreeSearch extends Singleton {
    public TreeSearch tree = new TreeSearch();

    @Override
    public Object toSave() {
        return null;
    }

    @Override
    public void reset() {
        tree = new TreeSearch();
    }

    @Override
    public Type type() {
        return null;
    }

    @Override
    public void toLoad(Object obj) {

    }
}
