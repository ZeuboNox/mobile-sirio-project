package com.example.payuapp.Singleton;

import android.app.ProgressDialog;

import com.example.payuapp.Article.Article;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

public class SingletonWaiting extends Singleton{
    public ProgressDialog progressDialog = null;

    @Override
    public Object toSave() {
        return null;
    }

    @Override
    public void reset() {
    }

    @Override
    public Type type() {
        return null;
    }

    @Override
    public void toLoad(Object obj) {

    }
}
