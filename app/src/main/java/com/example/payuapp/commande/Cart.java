package com.example.payuapp.commande;

import com.example.payuapp.Article.Image;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.bson.types.ObjectId;

import java.util.List;

/**
 * classe correspondant a un cart JSON du projet
 */
public class Cart {
    @SerializedName("id")
    @Expose
    private ObjectId id = null;
    @SerializedName("title")
    @Expose
    private String title = "";
    @SerializedName("images")
    @Expose
    private List<Image> images = null;
    @SerializedName("price")
    @Expose
    private float price = 0;
    @SerializedName("inStock")
    @Expose
    private int inStock = 0;
    @SerializedName("quantity")
    @Expose
    private int quantity = 1;

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getInStock() {
        return inStock;
    }

    public void setInStock(int inStock) {
        this.inStock = inStock;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
