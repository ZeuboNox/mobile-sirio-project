package com.example.payuapp.commande;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.payuapp.Article.Image;
import com.example.payuapp.R;
import com.example.payuapp.Singleton.Singleton;
import com.example.payuapp.Singleton.SingletonArticles;
import com.zozx.glider.Glider;

import java.text.SimpleDateFormat;
import java.util.List;

public class CartAdapter extends BaseAdapter {
    Context context;
    List<Cart> cart;
    LayoutInflater inflater;

    public CartAdapter(Context context, List<Cart> cart){
        this.context = context;
        this.cart = cart;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return cart.size();
    }

    @Override
    public Object getItem(int position) {
        return cart.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.cart_layout, null); // inflate the layout
        TextView title = convertView.findViewById(R.id.cart_title);
        TextView price = convertView.findViewById(R.id.cart_price);
        TextView quantity = convertView.findViewById(R.id.cart_quantity);
        ImageView img = convertView.findViewById(R.id.cart_image);

        title.setText(cart.get(i).getTitle());
        price.setText(String.format("%.2f",cart.get(i).getPrice())+" €");
        quantity.setText(""+cart.get(i).getQuantity());
        Glider.load(img, cart.get(i).getImages().get(0).getUrl());
        return convertView;
    }
}
