package com.example.payuapp.commande;

import com.example.payuapp.Article.Categorie;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.bson.types.ObjectId;

import java.util.Date;
import java.util.List;

/**
 * classe correspondant a une commande JSON du projet
 */
public class Commande {
    @SerializedName("id")
    @Expose
    private ObjectId idCommande = null;

    @SerializedName("dateOfPaiement")
    @Expose
    private Date dateOfPaiement = null;

    @SerializedName("address")
    @Expose
    private String address = "";

    @SerializedName("delivered")
    @Expose
    private boolean delivered = true;

    @SerializedName("mobile")
    @Expose
    private String mobile = "";

    @SerializedName("cart")
    @Expose
    private List<Cart> cart = null;

    @SerializedName("user")
    @Expose
    private ObjectId id = null;

    @SerializedName("createdAt")
    @Expose
    private Date createdAt = null;

    @SerializedName("updatedAt")
    @Expose
    private Date updatedAt = null;

    @SerializedName("total")
    @Expose
    private float total = 0;

    @SerializedName("paymentId")
    @Expose
    private String paymentId = "";

    public ObjectId getIdCommande() {
        return idCommande;
    }

    public void setIdCommande(ObjectId idCommande) {
        this.idCommande = idCommande;
    }

    public Date getDateOfPaiement() {
        return dateOfPaiement;
    }

    public void setDateOfPaiement(Date dateOfPaiement) {
        this.dateOfPaiement = dateOfPaiement;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String adresseDestinataire) {
        this.address = adresseDestinataire;
    }

    public boolean isDelivered() {
        return delivered;
    }

    public void setDelivered(boolean dateLivraison) {
        this.delivered = dateLivraison;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public List<Cart> getCart() {
        return cart;
    }

    public void setCart(List<Cart> idProduit) {
        this.cart = idProduit;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId idUser) {
        this.id = idUser;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }
}
