package com.example.payuapp.commande;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.payuapp.Article.Article;
import com.example.payuapp.R;
import com.example.payuapp.Singleton.Singleton;
import com.example.payuapp.Singleton.SingletonArticles;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Adapter pour afficher la liste des commandes dans la listView du fragment CommandeFragment
 */
public class CommandeAdapter extends BaseAdapter {
    Context context;
    List<Commande> commandes;
    LayoutInflater inflater;

    public CommandeAdapter(Context context, List<Commande> commandes){
        this.context = context;
        this.commandes = commandes;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return commandes.size();
    }

    @Override
    public Object getItem(int position) {
        return commandes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.commande_layout, null); // inflate the layout
        TextView title = convertView.findViewById(R.id.commande_title);
        TextView price = convertView.findViewById(R.id.commande_price);
        TextView number = convertView.findViewById(R.id.commande_number);
        TextView date = convertView.findViewById(R.id.commande_date);
        int len = commandes.get(i).getCart().size();

        title.setText(len == 1 ? len + " Article": len + " Articles");
        price.setText(String.format("%.2f",commandes.get(i).getTotal()) + "€");
        number.setText(commandes.get(i).getPaymentId());
        date.setText(new SimpleDateFormat("dd/MM/YYYY").format(commandes.get(i).getDateOfPaiement()));
        return convertView;
    }
}
