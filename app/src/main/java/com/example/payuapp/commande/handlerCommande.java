package com.example.payuapp.commande;

import android.util.Log;

import com.example.payuapp.API.RequestAPI;
import com.example.payuapp.API.HandlerAPI;
import com.example.payuapp.ArianneForFragment;
import com.example.payuapp.Parameter;
import com.example.payuapp.Singleton.Singleton;
import com.example.payuapp.Singleton.SingletonCommande;
import com.example.payuapp.connexion.User;
import com.example.payuapp.fragment.ValidPaymentFragment;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.bson.types.ObjectId;

import java.io.IOException;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

/**
 * classe pour pour executer les differente requete des commandes de l'utilisateur
 */
public class handlerCommande implements HandlerAPI<List<Commande>> {

    private Commande commande;

    final User user = (User) Singleton.getInstance(User.class);
    final ArianneForFragment arianne = (ArianneForFragment) Singleton.getInstance(ArianneForFragment.class);
    final SingletonCommande listCommandes = (SingletonCommande) Singleton.getInstance(SingletonCommande.class);

    public handlerCommande(){}

    /**
     * methode pour récuperer toutes les commandes de l'utilisateur
     * @param idUser: ObjectId, id de l'utilisateur connecté
     */
    public handlerCommande(ObjectId idUser){
            RequestAPI<List<Commande>> api = new RequestAPI<List<Commande>>();
            Parameter parms = new Parameter();
            parms.idUser = idUser;
            api.Api(this, "List<Commande>", parms);
    }

    /**
     * methode pour evoyer une nouvelle commande
     * @param cmd: Commande, la nouvelle commande
     * @throws IOException
     */
    public void postCommande( Commande cmd) throws IOException {
        commande = cmd;
        RequestAPI<ResponseBody> api = new RequestAPI<>();
        Parameter params = new Parameter();
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(cmd);
        Log.d("DEBUGAGE", json);
        params.request = RequestBody.create(MediaType.parse("application/json"), "["+json+"]");
        params.objUser = user;
        params.alwaysAwake = true;
        api.Api(new HandlerAPI<ResponseBody>() {
            @Override
            public void Awake(ResponseBody obj) {
                arianne.nextPage(new ValidPaymentFragment(commande));
            }
        }, "Commande", params);
    }

    @Override
    public void Awake(List<Commande> obj) {
        listCommandes.commandes = obj;
        Log.d("TESTAGE", obj.toString());
    }}
