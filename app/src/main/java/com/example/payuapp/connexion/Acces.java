package com.example.payuapp.connexion;

import com.google.gson.annotations.SerializedName;

/**
 * classe correspondant a un Acces JSON du projet
 */
public class Acces {
    @SerializedName("utilisateur")
    private User utilisateur = null;

    @SerializedName("idUtilisateur")
    private int idUtilisateur = 0;


    public User getUtilisateur() {
        return utilisateur;
    }

    public int getIdUtilisateur() {
        return idUtilisateur;
    }
}
