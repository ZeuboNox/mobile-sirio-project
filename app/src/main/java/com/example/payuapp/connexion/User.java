package com.example.payuapp.connexion;

import com.example.payuapp.Singleton.Singleton;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.bson.types.ObjectId;

import java.lang.reflect.Type;
import java.util.Date;

/**
 * classe correspondant a un utilisateur JSON du projet
 *
 * WARNING: l'id recuperé ne correspond pas toujours a l'utilisateur,
 * se qui provoque l'echec de la recuperation des commandes
 */
public class User extends Singleton {
    //MODE DEBUG permet de se connecter avec un unique utilisateur dont l'id correspond a la base de donnée
    boolean modeOneTrickUser = false;

    @SerializedName("adresse")
    @Expose
    private String adresse = "";
    @SerializedName("codePostal")
    @Expose
    private String codePostal = "";
    @SerializedName("complement")
    @Expose
    private String complement  = "";
    @SerializedName("email")
    @Expose
    private String email = "";
    @SerializedName("id")
    @Expose
    private ObjectId idUtilisateur = null;
    @SerializedName("instructionLivraison")
    @Expose
    private String instructionLivraison = "";
    @SerializedName("name")
    @Expose
    private String nom = "";
    @SerializedName("avatar")
    @Expose
    private String avatar = null;
    @SerializedName("paysRegion")
    @Expose
    private String paysRegion = "";
    @SerializedName("prenom")
    @Expose
    private String prenom = "";
    @SerializedName("sexe")
    @Expose
    private String sexe = "";
    @SerializedName("telephone")
    @Expose
    private String telephone = "";
    @SerializedName("ville")
    @Expose
    private String ville = "";
    @SerializedName("createdAt")
    @Expose
    private Date createdAt = null;
    @SerializedName("updatedAt")
    @Expose
    private Date updatedAt = null;


    public String getAdresse() {
        return adresse;
    }
    public void setAdresse(String adresse) {this.adresse = adresse; }

    public String getCodePostal() {
        return codePostal;
    }
    public void setCodePostal(String codePostal) {this.codePostal = codePostal; }

    public String getComplement() {
        return complement;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {this.email = email; }

    public ObjectId getId(){return idUtilisateur;}
    public int getIdUtilisateur() {
        try {
            return (int)idUtilisateur.getDate().getTime();
        } catch(Exception e){
            return -1;
        }
    }

    public String getInstructionLivraison() {
        return instructionLivraison;
    }

    public String getNom() {
        return nom;
    }
    public void setNom(String nom) {this.nom = nom; }

    public String getPaysRegion() {
        return paysRegion;
    }
    public void setPaysRegion(String paysRegion) {this.paysRegion = paysRegion; }

    public String getPrenom() {
        return prenom;
    }
    public void setPrenom(String prenom) {this.prenom = prenom; }

    public String getSexe() {
        return sexe;
    }

    public String getTelephone() {
        return telephone;
    }
    public void setTelephone(String telephone) {this.telephone = telephone; }

    public String getVille() {
        return ville;
    }
    public void setVille(String ville) {this.ville = ville; }

    public String toString(){
        return idUtilisateur + "\n" + nom + "\n" + prenom + "\n" + sexe + "\n" + email + "\n\n" +adresse+ "\n" + ville + " " + codePostal + "\n" + paysRegion;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }


    @Override
    public User toSave() {
        return this;
    }

    @Override
    public void reset() {
        prenom = "";
        idUtilisateur = null;
        ville = "";
        nom = "";
        adresse = "";
        avatar = null;
        codePostal = "";
        complement = "";
        createdAt = null;
        email = "";
        instructionLivraison = "";
        paysRegion = "";
        sexe = "";
        telephone = "";
        updatedAt = null;
    }

    @Override
    public Type type() {
        return null;
    }

    @Override
    public void toLoad(Object obj) {

        User loadingUser = (User)obj;
        prenom = loadingUser.getPrenom();
        if(modeOneTrickUser){
            ObjectId ob = new ObjectId("607d7e6001bd8679f1fc1d41");
            idUtilisateur = ob;
        }else{
            idUtilisateur = loadingUser.getId();
        }
        ville = loadingUser.getVille();
        nom = loadingUser.getNom();
        adresse = loadingUser.getAdresse();
        avatar = loadingUser.getAvatar();
        codePostal = loadingUser.getCodePostal();
        complement = loadingUser.getComplement();
        createdAt = loadingUser.getCreatedAt();
        email = loadingUser.getEmail();
        instructionLivraison = loadingUser.getInstructionLivraison();
        paysRegion = loadingUser.getPaysRegion();
        sexe = loadingUser.getSexe();
        telephone = loadingUser.getTelephone();
        updatedAt = loadingUser.getUpdatedAt();

    }
}
