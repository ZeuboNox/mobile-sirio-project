package com.example.payuapp.favoris;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.payuapp.ArianneForFragment;
import com.example.payuapp.Article.Article;
import com.example.payuapp.R;
import com.example.payuapp.Singleton.Singleton;
import com.example.payuapp.fragment.ArticleFragment;
import com.example.payuapp.fragment.FavorisFragment;
import com.example.payuapp.panier.Panier;
import com.zozx.glider.Glider;

import java.util.List;

/**
 * Adapter pour afficher la liste des articles favoris dans la listview du fragment FavorisFragment
 */
public class FavorisAdapter extends BaseAdapter {
    Context context;
    List<Article> articles;
    LayoutInflater inflater;
    FavorisFragment favoris;

    final ArianneForFragment arianne = (ArianneForFragment) Singleton.getInstance(ArianneForFragment.class);
    final Panier panier = (Panier) Singleton.getInstance(Panier.class);

    int[] number;

    public FavorisAdapter(Context context, List<Article> articles, FavorisFragment favoris){
        number = new int[articles.size()];
        for(int i=0; i<number.length;i++){
            number[i] = 1;
        }
        this.context = context;
        this.articles = articles;
        inflater = LayoutInflater.from(context);
        this.favoris = favoris;
    }

    @Override
    public int getCount() {
        return articles.size();
    }

    @Override
    public Object getItem(int position) {
        return articles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.favoris_layout, null); // inflate the layout
        TextView title = convertView.findViewById(R.id.favoris_title);
        TextView desc = convertView.findViewById(R.id.favoris_description);
        TextView price = convertView.findViewById(R.id.favoris_price);
        ImageButton del = convertView.findViewById(R.id.delete_favoris);
        ImageButton pan = convertView.findViewById(R.id.panier_favoris);
        ImageView img = convertView.findViewById(R.id.favoris_image);

        Glider.load(img, articles.get(i).getImages().get(0).getUrl());
        LinearLayout onclick = convertView.findViewById(R.id.favoris_onclick);

        String lib = articles.get(i).getLibelle();
        String des = articles.get(i).getDescription();
        title.setText(lib.length()>= 20 ? lib.substring(0, 20)+"...": lib);
        desc.setText(des.length()>= 100 ? des.substring(0, 100)+"...": des);
        price.setText(articles.get(i).getPrix().toString() + "€");

        onclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArticleFragment articleFrag = new ArticleFragment();
                articleFrag.setArticle(articles.get(i));
                arianne.nextPage(articleFrag);
            }
        });

        del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                articles.get(i).inFavoris = false;
                articles.remove(articles.get(i));
                favoris.awake();
            }
        });
        if(!articles.get(i).inPanier){
            pan.setEnabled(true);
            pan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    articles.get(i).inPanier = true;
                    panier.add(articles.get(i));
                    favoris.awake();
                }
            });
        }else{
            pan.setEnabled(false);
        }

        onclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arianne.nextPage(new ArticleFragment(articles.get(i)));
            }
        });


        return convertView;
    }

}
