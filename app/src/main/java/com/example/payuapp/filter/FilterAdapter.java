package com.example.payuapp.filter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.example.payuapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter pour afficher la liste des mot de triage dans la gridview du fragment FilterFragment
 */
public class FilterAdapter extends BaseAdapter {

    Context context;
    List<String> triage;
    ArrayList<TextView> triButton;
    LayoutInflater inflater;

    public FilterAdapter(Context context, List<String> triage){
            this.context = context;
            this.triage = triage;
            inflater = LayoutInflater.from(context);
            triButton = new ArrayList<TextView>();
            }

    @Override
    public int getCount() {
            return triage.size();
            }

    @Override
    public Object getItem(int position) {
            return triage.get(position);
            }

    @Override
    public long getItemId(int position) {
            return 0;
            }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
            convertView = inflater.inflate(R.layout.filter_layout, null); // inflate the layout
            TextView button = convertView.findViewById(R.id.tri_filter_button);
            button.setText(triage.get(i));
            triButton.add(button);
            return convertView;
            }
}
