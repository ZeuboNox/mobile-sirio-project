package com.example.payuapp.filter;

import android.widget.TextView;

/**
 * Enumerateur pour obtenir les string des nom de triage (utilisation pedagogique, ce n'est pas forcement optimal pour le projet)
 */
public enum FiltreOption {
    NONE("Empty"),PRIX_CROISSANT("Prix Croissant"), PRIX_DECROISSANT("Prix Decroissant"), NOM("Nom"), NOTE("Note");

    public String name;
    private TextView txt;

    FiltreOption(String s) {
        name = s;
    }

    public String getName(){
        return name;
    }

    public TextView getTxt() {
        return txt;
    }

    public void setTxt(TextView txt) {
        this.txt = txt;
    }
}
