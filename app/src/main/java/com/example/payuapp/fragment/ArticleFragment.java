package com.example.payuapp.fragment;

import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.payuapp.Article.Article;
import com.example.payuapp.Article.ImageAdapter;
import com.example.payuapp.R;
import com.example.payuapp.Singleton.Singleton;
import com.example.payuapp.Singleton.SingletonFavoris;
import com.example.payuapp.panier.Panier;

/**
 * Affiche le contenue d'un article
 *
 * A simple {@link Fragment} subclass.
 * Use the {@link ArticleFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ArticleFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    Article article;

    ViewPager pager;
    Button buttonPanier;
    ImageView buttonFavoris;

    final Panier panier = (Panier) Singleton.getInstance(Panier.class);
    final SingletonFavoris singleFav = (SingletonFavoris) Singleton.getInstance(SingletonFavoris.class);

    public ArticleFragment() {
        // Required empty public constructor
    }

    public ArticleFragment(Article article) {
        this.article = article;
    }

    public void setArticle(Article article){
        this.article = article;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ArticleFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ArticleFragment newInstance(String param1, String param2) {
        ArticleFragment fragment = new ArticleFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_article, container, false);
        buttonPanier = view.findViewById(R.id.buttonInteractBottom);
        buttonFavoris = view.findViewById(R.id.image_favoris);
        pager = view.findViewById(R.id.pager_article);

        makePanierButton();
        makeFavorisButton();

        ImageAdapter imgAdapt = new ImageAdapter(getContext(), article.getImages());
        pager.setAdapter(imgAdapt);

        TextView title = view.findViewById(R.id.title_detail_article);
        title.setText(article.getLibelle());
        TextView description = view.findViewById(R.id.description_detail_article);
        description.setText(article.getDescription());
        TextView price = view.findViewById(R.id.price_detail_article);
        price.setText(article.getPrix() + " €");
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        buttonPanier.getLayoutParams().height = 1;
    }

    /**
     * methode qui fabrique le boutton qui envoie ou nous un articles au panier
     *
     * une verification est effectué dans le cas ou l'article est déjà présent dans le panier ou non
     * si il n'est pas présent dans le panier, le button ajouteras l'article dans le panier
     * sinon le boutton retireras l'article du panier
     */
    public void makePanierButton(){
        if(!article.inPanier){
            buttonPanier.setText("Ajouter au panier");
            buttonPanier.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getContext(), "L'article a été ajouté", Toast.LENGTH_SHORT).show();
                    panier.add(article);
                    article.inPanier = true;
                    makePanierButton();
                }
            });
        }else{
            buttonPanier.setText("Retirer du panier");
            buttonPanier.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getContext(), "L'article a été supprimé", Toast.LENGTH_SHORT).show();
                    panier.remove(article);
                    article.inPanier = false;
                    makePanierButton();
                }
            });
        }

    }

    /**
     * methode qui fabrique le boutton qui envoie ou nous un articles au favoris
     *
     * une verification est effectué dans le cas ou l'article est déjà présent dans les favoris ou non
     * si il n'est pas présent dans les favoris, le button ajouteras l'article dans le panier
     * sinon le boutton retireras l'article du favoris
     */
    public void makeFavorisButton(){
        if(!article.inFavoris){
            buttonFavoris.setColorFilter(ContextCompat.getColor(getContext(), R.color.grey_disabled), android.graphics.PorterDuff.Mode.SRC_IN);
            buttonFavoris.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getContext(), "L'article est en favoris", Toast.LENGTH_SHORT).show();
                    singleFav.articlesFavoris.add(article);
                    article.inFavoris = true;
                    Singleton.save(getContext(), SingletonFavoris.class);
                    makeFavorisButton();
                }
            });
        }else{
            buttonFavoris.setColorFilter(ContextCompat.getColor(getContext(), R.color.purple_200), android.graphics.PorterDuff.Mode.SRC_IN);
            buttonFavoris.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(getContext(), "L'article n'est plus en favoris", Toast.LENGTH_SHORT).show();
                    singleFav.articlesFavoris.remove(article);
                    article.inFavoris = false;
                    Singleton.save(getContext(), SingletonFavoris.class);
                    makeFavorisButton();
                }
            });
        }
    }

}