package com.example.payuapp.fragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.example.payuapp.ArianneForFragment;
import com.example.payuapp.BackButton;
import com.example.payuapp.PaypalClientIDConfig;
import com.example.payuapp.R;
import com.example.payuapp.Singleton.Singleton;
import com.example.payuapp.commande.Cart;
import com.example.payuapp.commande.Commande;
import com.example.payuapp.commande.handlerCommande;
import com.example.payuapp.connexion.User;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

/**
 * affiche une verification des informations avant le payment
 *
 * WARNING: le systeme de payment paypal est bien en place cependant il ne fonctionne pas
 * il est impossible de reussire un payment, c'est pour cela q'un texte forcant la reussite est présent pour les test
 * mais aucun payment n'est effectuer sur le compte paypal de test
 *
 * A simple {@link Fragment} subclass.
 * Use the {@link CheckFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CheckFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    final User user = (User) Singleton.getInstance(User.class);
    final ArianneForFragment arianne = (ArianneForFragment) Singleton.getInstance(ArianneForFragment.class);

    private float price;
    private Cart[] commandeProduits;
    TextView textView;
    ViewSwitcher switcherName;
    ViewSwitcher switcherAddress;
    ViewSwitcher switcherLocation;
    Button buttonStep;
    Button buttonInteract;

    int PAYPAL_REQ_CODE = 12;
    PayPalConfiguration paypalConfig = new PayPalConfiguration()
            .environment(PayPalConfiguration.ENVIRONMENT_SANDBOX)
            .clientId(PaypalClientIDConfig.PAYPAL_CLIENT_ID);

    public CheckFragment() {
        // Required empty public constructor
        price = -1;
    }

    public CheckFragment(float price, Cart[] commandeProduits) {
        this.price = price;
        this.commandeProduits = commandeProduits;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CheckFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CheckFragment newInstance(String param1, String param2) {
        CheckFragment fragment = new CheckFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        if(user == null)
            Singleton.load(getContext(), User.class, false);

        Intent intent = new Intent(getContext(), PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, paypalConfig);
        getActivity().startService(intent);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Singleton.load(getContext(), User.class, false);
        View view = inflater.inflate(R.layout.fragment_check, container, false);
        textView = view.findViewById(R.id.debug_valid_payment);
        switcherAddress = view.findViewById(R.id.switcher_address_check);
        switcherName = view.findViewById(R.id.switcher_name_check);
        switcherLocation = view.findViewById(R.id.switcher_location_check);
        buttonStep = view.findViewById(R.id.button_step_check);
        buttonInteract = getActivity().findViewById(R.id.buttonInteract);
        BackButton.active(buttonInteract);

        initTextValue();
        makeModifButton();
        makeDebugValidPayment();
        makeNextStepButton();

        return view;
    }


    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == PAYPAL_REQ_CODE){
            if(resultCode == getActivity().RESULT_OK){
                successNextStep();
            }else{
                arianne.nextPage(new BadPaymentFragment());
            }
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        BackButton.disable(buttonInteract);
        getActivity().stopService(new Intent(getContext(), PayPalService.class));
    }

    /**
     * button qui demarre le moyen de payment paypal
     */
    private void makeNextStepButton(){
        buttonStep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BackButton.disable(buttonInteract);
                paypalPaymentsMethod();
            }
        });
    }

    /**
     * button qui active la modification des informations de l'utilisateur
     */
    private void makeModifButton(){
        buttonInteract.setText("MODIFIER");
        buttonInteract.setTextSize(20);
        buttonInteract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonStep.setEnabled(false);
                switcherName.showNext();
                switcherAddress.showNext();
                switcherLocation.showNext();
                makeValideButton();

                switchLayoutToTextView(false);
            }
        });
    }

    /**
     * button qui valide la modification des informations de l'utilisateur
     */
    private void makeValideButton(){
        buttonInteract.setText("VALIDER");
        buttonInteract.setTextSize(20);
        buttonInteract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                buttonStep.setEnabled(true);
                EditText etext = switcherAddress.findViewById(R.id.edit_address_check);
                TextView text = switcherAddress.findViewById(R.id.text_address_check);
                text.setText(etext.getText());
                etext = switcherName.findViewById(R.id.edit_name_check);
                text = switcherName.findViewById(R.id.text_name_check);
                text.setText(etext.getText());
                etext = switcherLocation.findViewById(R.id.edit_location_check);
                text = switcherLocation.findViewById(R.id.text_location_check);
                text.setText(etext.getText());

                switcherName.showNext();
                switcherAddress.showNext();
                switcherLocation.showNext();
                makeModifButton();

                switchLayoutToTextView(true);
            }
        });
    }

    /**
     * methode qui initialise les informations de l'utilisateur
     */
    private void initTextValue(){
        if(user == null){
            return;
        }
        boolean ableButton = false;
        TextView text = switcherAddress.findViewById(R.id.text_address_check);
        EditText etext = switcherAddress.findViewById(R.id.edit_address_check);

        if(user.getAdresse() != ""){
            text.setText(user.getAdresse());
            etext.setText(user.getAdresse());
            ableButton = true;
        }
        text = switcherName.findViewById(R.id.text_name_check);
        etext = switcherName.findViewById(R.id.edit_name_check);
        if(user.getPrenom()!= "" || user.getNom() != ""){
            text.setText(user.getPrenom() +" "+user.getNom());
            etext.setText(user.getPrenom() +" "+user.getNom());
            ableButton = true;
        }

        text = switcherLocation.findViewById(R.id.text_location_check);
        etext = switcherLocation.findViewById(R.id.edit_location_check);
        if(user.getCodePostal() != "" && user.getVille() != ""){
            text.setText(user.getCodePostal() +" "+ user.getVille());
            etext.setText(user.getCodePostal() +" "+ user.getVille());
            ableButton = true;
        }

        buttonStep.setEnabled(ableButton);
    }

    /**
     * methode qui switch entre TextView et EditView les informations de l'utilisateur
     * @param etat
     */
    private void switchLayoutToTextView(boolean etat){
        EditText etext = switcherAddress.findViewById(R.id.edit_address_check);
        TextView text = switcherAddress.findViewById(R.id.text_address_check);
        backEtat(etat, etext, text);
        etext = switcherName.findViewById(R.id.edit_name_check);
        text = switcherName.findViewById(R.id.text_name_check);
        backEtat(etat, etext, text);
        etext = switcherLocation.findViewById(R.id.edit_location_check);
        text = switcherLocation.findViewById(R.id.text_location_check);
        backEtat(etat, etext, text);
    }
    private void backEtat(boolean etat, EditText etext, TextView text){
        if(etat){
            BackButton.disable(etext);
            BackButton.active(text);
        }else {
            BackButton.active(etext);
            BackButton.disable(text);
        }
    }

    /**
     * DEBUG METHODE
     * methode pour reussir sans echec la validation du payment
     */
    void makeDebugValidPayment(){
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                successNextStep();
            }
        });
    }

    /**
     * methode qui envoie vers paypal
     */
    private void paypalPaymentsMethod(){
        PayPalPayment payments = new PayPalPayment(new BigDecimal(price),"EUR", "Test Payments payu", PayPalPayment.PAYMENT_INTENT_SALE);

        Intent intent = new Intent(getContext(), PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, paypalConfig);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payments);

        startActivityForResult(intent, PAYPAL_REQ_CODE);
    }

    /**
     * creation d'une nouvelle commande
     */
    private void successNextStep(){
        BackButton.disable(buttonInteract);
        Commande commande = new Commande();
        TextView address = switcherAddress.findViewById(R.id.text_address_check);
        TextView location = switcherLocation.findViewById(R.id.text_location_check);
        commande.setAddress(address.getText().toString() + " " + location.getText().toString());
        commande.setId(user.getId());
        commande.setMobile(user.getTelephone());
        commande.setPaymentId("SKIPPED PAYMENT");
        commande.setCart(new ArrayList<Cart>(Arrays.asList(commandeProduits)));
        commande.setTotal(price);
        commande.setDateOfPaiement(new Date());
        commande.setMobile("");
        try {
            new handlerCommande().postCommande(commande);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}