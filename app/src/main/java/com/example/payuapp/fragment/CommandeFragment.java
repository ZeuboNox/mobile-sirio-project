package com.example.payuapp.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.payuapp.ArianneForFragment;
import com.example.payuapp.R;
import com.example.payuapp.Singleton.Singleton;
import com.example.payuapp.Singleton.SingletonCommande;
import com.example.payuapp.commande.Commande;
import com.example.payuapp.commande.CommandeAdapter;

import java.util.List;

/**
 * affiche la liste de toute les commandes d'un utilisateur
 *
 * A simple {@link Fragment} subclass.
 * Use the {@link CommandeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CommandeFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ListView listCommande;

    final SingletonCommande singleCom = (SingletonCommande) Singleton.getInstance(SingletonCommande.class);
    final ArianneForFragment arianne = (ArianneForFragment) Singleton.getInstance(ArianneForFragment.class);

    public CommandeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CommandeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CommandeFragment newInstance(String param1, String param2) {
        CommandeFragment fragment = new CommandeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_commande, container, false);
        listCommande = view.findViewById(R.id.list_commande);
        makeListCommande();
        return view;
    }

    /**
     * créé la liste des commandes de haut en bas
     */
    private void makeListCommande(){
        List<Commande> commandes = singleCom.commandes;
        CommandeAdapter customAdapter = new CommandeAdapter(getContext(), commandes);
        listCommande.setAdapter(customAdapter);

        listCommande.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int i, long id) {
                arianne.nextPage(new DetailCommandeFragment(singleCom.commandes.get(i)));
            }
        });
    }
}