package com.example.payuapp.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.payuapp.API.RequestAPI;
import com.example.payuapp.API.HandlerAPI;
import com.example.payuapp.ArianneForFragment;
import com.example.payuapp.Parameter;
import com.example.payuapp.R;
import com.example.payuapp.Singleton.Singleton;
import com.example.payuapp.Singleton.SingletonWaiting;
import com.example.payuapp.commande.handlerCommande;
import com.example.payuapp.connexion.User;

/**
 * formulaire de connexion
 *
 * A simple {@link Fragment} subclass.
 * Use the {@link ConnexionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ConnexionFragment extends Fragment implements HandlerAPI<User> {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    final User user = (User) Singleton.getInstance(User.class);
    final ArianneForFragment arianne = (ArianneForFragment) Singleton.getInstance(ArianneForFragment.class);
    final SingletonWaiting waiter = (SingletonWaiting) Singleton.getInstance(SingletonWaiting.class);

    EditText userField;
    EditText passwordField;
    Button button;

    public ConnexionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ConnexionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ConnexionFragment newInstance(String param1, String param2) {
        ConnexionFragment fragment = new ConnexionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    /**
     * instancie le bouton pour effectuer la connexion
     * @param view
     */
    public void connexionSys(View view){
        userField = view.findViewById(R.id.user);
        passwordField = view.findViewById(R.id.password);
        button = view.findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("CREATION", "connexion button");
                waiter.progressDialog = ProgressDialog.show(getContext(),"Connexion", "patientez...",false,false);
                RequestAPI<User> api = new RequestAPI<User>();
                Parameter parms = new Parameter();
                parms.user = userField.getText().toString();
                parms.pass = passwordField.getText().toString();
                api.Api(ConnexionFragment.this, "User", parms);
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_connexion, container, false);
        if(user.getId() == null){
            connexionSys(view);
        }
        return view;
    }

    @Override
    public void Awake(User obj) {
        waiter.progressDialog.dismiss();
        if (obj != null) {
            user.toLoad(obj);
            new handlerCommande(obj.getId());
            Singleton.save(getContext(), User.class);
            arianne.startPage(new OptionUserFragment());
        };
    }
}