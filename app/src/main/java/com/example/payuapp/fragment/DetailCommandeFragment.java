package com.example.payuapp.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.example.payuapp.R;
import com.example.payuapp.commande.CartAdapter;
import com.example.payuapp.commande.Commande;

import java.text.SimpleDateFormat;

/**
 * affcihe les details d'une commande
 *
 * A simple {@link Fragment} subclass.
 * Use the {@link DetailCommandeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailCommandeFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    Commande commande;

    TextView idCommande;
    TextView date;
    TextView livraison;
    ListView cart;
    TextView total;

    public DetailCommandeFragment() {
        // Required empty public constructor
    }

    public DetailCommandeFragment(Commande commande) {
        this.commande =commande;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DetailCommandeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailCommandeFragment newInstance(String param1, String param2) {
        DetailCommandeFragment fragment = new DetailCommandeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_detail_commande, container, false);
        idCommande = view.findViewById(R.id.detail_commande_id);
        date = view.findViewById(R.id.detail_commande_date);
        livraison = view.findViewById(R.id.detail_commande_livraison);
        cart = view.findViewById(R.id.detail_commande_list);
        total = view.findViewById(R.id.detail_commande_total);

        initTextView();
        makeListDetail();

        return view;
    }

    private void initTextView(){
        idCommande.setText(commande.getPaymentId());
        date.setText(new SimpleDateFormat("dd/MM/YYYY").format(commande.getDateOfPaiement()));
        livraison.setText(commande.getAddress());
        total.setText(String.format("%.2f",commande.getTotal())+" €");
    }

    private void makeListDetail(){
        CartAdapter adapter = new CartAdapter(getContext(), commande.getCart());
        cart.setAdapter(adapter);
    }
}