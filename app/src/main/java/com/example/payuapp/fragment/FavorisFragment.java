package com.example.payuapp.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.payuapp.Article.Article;
import com.example.payuapp.R;
import com.example.payuapp.Singleton.Singleton;
import com.example.payuapp.Singleton.SingletonFavoris;
import com.example.payuapp.favoris.FavorisAdapter;
import com.example.payuapp.panier.PanierAdaptater;

import java.util.ArrayList;

/**
 * affiche la liste des articles favoris
 *
 * A simple {@link Fragment} subclass.
 * Use the {@link FavorisFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FavorisFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ListView list;

    final SingletonFavoris singleFav = (SingletonFavoris)Singleton.getInstance(SingletonFavoris.class);

    public FavorisFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FavorisFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FavorisFragment newInstance(String param1, String param2) {
        FavorisFragment fragment = new FavorisFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favoris, container, false);
        list = view.findViewById(R.id.list_favoris);
        makeList();
        return view;
    }

    private void makeList(){
        Singleton.save(getContext(), SingletonFavoris.class);
        FavorisAdapter customAdapter = new FavorisAdapter(getContext(), singleFav.articlesFavoris, this);
        list.setAdapter(customAdapter);
    }

    public void awake(){
        makeList();
    }
}