package com.example.payuapp.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.payuapp.ArianneForFragment;
import com.example.payuapp.Article.Article;
import com.example.payuapp.R;
import com.example.payuapp.Singleton.Singleton;
import com.example.payuapp.Singleton.SingletonArticles;
import com.example.payuapp.filter.FilterAdapter;
import com.example.payuapp.filter.FiltreOption;
import com.google.android.material.slider.RangeSlider;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * permet de selectionner le filtre que l'on souhaite sur une liste d'article
 *
 * A simple {@link Fragment} subclass.
 * Use the {@link FiltreFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FiltreFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    final ArianneForFragment arianne = (ArianneForFragment) Singleton.getInstance(ArianneForFragment.class);
    final SingletonArticles singleArt = (SingletonArticles) Singleton.getInstance(SingletonArticles.class);

    GridView triFilter;
    RangeSlider sliderFilter;
    Button resultFilter;
    Spinner spinner;

    String categorieSelect;

    List<Article> articlesSelected;
    List<Article> articlesOrigine;

    FiltreOption optionSelected;

    public FiltreFragment() {
        // Required empty public constructor
    }

    /**
     * constructeur appeler lorsque qu'on selectionne une liste spécifique d'articles
     * @param articles: List<Article>, liste des articles qui seront filtré
     */
    public FiltreFragment(List<Article> articles) {
        articlesOrigine = articles;
        articlesSelected = articlesOrigine == null? null: new ArrayList<>(articlesOrigine);
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FiltreFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FiltreFragment newInstance(String param1, String param2) {
        FiltreFragment fragment = new FiltreFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        categorieSelect = "";

        View view = inflater.inflate(R.layout.fragment_filtre, container, false);
        triFilter = view.findViewById(R.id.tri_filter);
        resultFilter = view.findViewById(R.id.result_filter);
        sliderFilter = view.findViewById(R.id.slider_filter);
        spinner = view.findViewById(R.id.spinner_filter);
        optionSelected = FiltreOption.NONE;

        makeSpinner();
        makeTriFilter();
        setFilterButton();
        return view;
    }

    /**
     * créé les options de triage des liste par:
     * - PRIX_CROISSANT
     * - PRIX_DECROISSANT
     * - NOM
     * - NOTE
     */
    public void makeTriFilter(){
        ArrayList<String> noms = new ArrayList<String>();
        noms.add(FiltreOption.PRIX_CROISSANT.name);
        noms.add(FiltreOption.PRIX_DECROISSANT.name);
        noms.add(FiltreOption.NOM.name);
        noms.add(FiltreOption.NOTE.name);
        FilterAdapter adapter = new FilterAdapter(getContext(),noms);
        triFilter.setAdapter(adapter);
        triFilter.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(articlesSelected == null)
                    return;
                TextView txt = view.findViewById(R.id.tri_filter_button);

                if(optionSelected.getTxt() != null){
                    optionSelected.getTxt().setBackgroundColor(getResources().getColor(R.color.purple_500));
                }

                switch (txt.getText().toString()){
                    case "Prix Croissant":
                        setOptionSelected(FiltreOption.PRIX_CROISSANT);
                        break;
                    case "Prix Decroissant":
                        setOptionSelected(FiltreOption.PRIX_DECROISSANT);
                        break;
                    case "Nom":
                        setOptionSelected(FiltreOption.NOM);
                        break;
                    case "Note":
                        setOptionSelected(FiltreOption.NOTE);
                        break;
                }

                if(optionSelected != FiltreOption.NONE){
                    txt.setBackgroundColor(getResources().getColor(R.color.purple_200));
                }
                optionSelected.setTxt(txt);
            }
        });
    }

    /**
     * methode qui instancie la selection d'une tranche de prix
     */
    private void makeSpinner(){
        ArrayAdapter<String> adapter = new ArrayAdapter(getContext(), R.layout.txt_spinner, singleArt.getStringCategorie());
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                categorieSelect = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                categorieSelect = "";
            }
        });
    }

    private void setOptionSelected(FiltreOption option){
        if(optionSelected == option){
            optionSelected = FiltreOption.NONE;
            return;
        }
        optionSelected = option;
    }

    /**
     * methode qui instancie le boutton pour effectué le triage + filtrage et renvoie vers un HomeFragment
     */
    public  void setFilterButton(){
        resultFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(articlesOrigine == null){
                    arianne.nextPage(new HomeFragment());
                    return;
                }
                articlesSelected = new ArrayList<>(articlesOrigine);
                if(optionSelected == FiltreOption.PRIX_CROISSANT){
                    Collections.sort(articlesSelected, new SortByPrice());
                } else if(optionSelected == FiltreOption.PRIX_DECROISSANT){
                    Collections.sort(articlesSelected, new SortReverseByPrice());
                } else if(optionSelected == FiltreOption.NOM){
                    Collections.sort(articlesSelected, new SortByName());
                }


                SelectedSlideFilter();
                arianne.nextPage(new HomeFragment((ArrayList<Article>) articlesSelected));
            }
        });
    }

    public void SelectedCategorie(){
        if(categorieSelect == ""){
            return;
        }
        ArrayList<Article> toKeep = new ArrayList<Article>();

        for(Article art:articlesSelected){
            if(art.getCategorie() == categorieSelect){
                toKeep.add(art);
            }
        }

        articlesSelected = toKeep;

    }

    /**
     * methode qui retire les article qui ne sont pas dans la tranche du prix
     */
    public void SelectedSlideFilter(){
        ArrayList<Article> toRemove = new ArrayList<Article>();
        float min = sliderFilter.getValues().get(0);
        float max = sliderFilter.getValues().get(1);
        for(Article art:articlesSelected){
            if(Float.parseFloat(art.getPrix()) < min || Float.parseFloat(art.getPrix()) > max){
                toRemove.add(art);
            }
        }

        for(Article art : toRemove){
            articlesSelected.remove(art);
        }
    }

    /************************************************************
     * Systeme pour effectuer un trie dans une liste d'articles *
     ************************************************************/
    static class SortByPrice implements Comparator<Article>{

        @Override
        public int compare(Article o1, Article o2) {
            return o1.getPrix().compareTo(o2.getPrix());
        }
    }
    static class SortReverseByPrice implements Comparator<Article>{

        @Override
        public int compare(Article o1, Article o2) {
            return o2.getPrix().compareTo(o1.getPrix());
        }
    }
    static class SortByName implements Comparator<Article>{

        @Override
        public int compare(Article o1, Article o2) {
            return o1.getLibelle().compareTo(o2.getLibelle());
        }
    }
}