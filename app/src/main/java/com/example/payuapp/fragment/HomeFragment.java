package com.example.payuapp.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;

import com.example.payuapp.ArianneForFragment;
import com.example.payuapp.Article.Article;
import com.example.payuapp.Article.ArticleAdaptater;
import com.example.payuapp.BackButton;
import com.example.payuapp.API.RequestAPI;
import com.example.payuapp.API.HandlerAPI;
import com.example.payuapp.Parameter;
import com.example.payuapp.R;
import com.example.payuapp.Singleton.Singleton;
import com.example.payuapp.Singleton.SingletonArticles;
import com.example.payuapp.Singleton.SingletonFavoris;
import com.example.payuapp.Singleton.SingletonTreeSearch;
import com.example.payuapp.Singleton.SingletonWaiting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * affiche la liste des articles
 *
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment implements HandlerAPI<List<Article>> {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    final ArianneForFragment arianne = (ArianneForFragment) Singleton.getInstance(ArianneForFragment.class);
    final SingletonArticles singleArt = (SingletonArticles) Singleton.getInstance(SingletonArticles.class);
    final SingletonWaiting waiter = (SingletonWaiting) Singleton.getInstance(SingletonWaiting.class);
    final SingletonFavoris singleFav = (SingletonFavoris) Singleton.getInstance(SingletonFavoris.class);
    final SingletonTreeSearch singleTree = (SingletonTreeSearch) Singleton.getInstance(SingletonTreeSearch.class);

    private GridView gridView;

    private ArrayList<Article> articlesSelected;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * constructeur permetant de selectionner les articles a afficher
     * @param articlesSelected: ArrayList<Article>, la liste des articles
     */
    public HomeFragment(ArrayList<Article> articlesSelected) {
        this.articlesSelected = articlesSelected;
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        gridView = view.findViewById(R.id.articles);
        if(singleArt.articles == null){
            waiter.progressDialog = ProgressDialog.show(getContext(),"recherche article", "patientez...",false,false);
            RequestAPI<List<Article>> api = new RequestAPI<List<Article>>();
            api.Api(this, "List<Article>", new Parameter());
        }else{
            makeGrid(articlesSelected == null ? singleArt.articles : articlesSelected);
        }
        makeFilter();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Button buttonInteract = getActivity().findViewById(R.id.buttonInteract);
        BackButton.disable(buttonInteract);
    }

    /**
     * methode qui instancie la grille des articles a afficher
     * @param articles
     */
    private void makeGrid(List<Article> articles){
        ArticleAdaptater customAdapter = new ArticleAdaptater(getContext(), articles);
        gridView.setAdapter(customAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int i, long id) {

                ArticleFragment articleFrag = new ArticleFragment();
                articleFrag.setArticle(articles.get(i));
                arianne.nextPage(articleFrag);
            }
        });
    }

    /**
     * methode qui créé le boutton pour filtrer les articles
     */
    public void makeFilter(){
        Button buttonInteract = getActivity().findViewById(R.id.buttonInteract);
        BackButton.active(buttonInteract);
        buttonInteract.setText("FILTRE");
        buttonInteract.setTextSize(20);
        buttonInteract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arianne.nextPage(new FiltreFragment(articlesSelected == null ? singleArt.articles: articlesSelected));
            }
        });
    }

    @Override
    public void Awake(List<Article> obj) {
        waiter.progressDialog.dismiss();
        singleArt.articlesDico = new HashMap<Integer, Article>();
        Log.d("CREATION", ""+(obj.get(0).getLibelle() == null));
        for( int i=0; i< obj.size();i++){
            obj.get(i).makeTag();
            singleTree
                    .tree
                    .addWord(obj
                            .get(i)
                            .getTreeTag()
                            .getAllWord());
            singleArt.articlesDico.put(obj.get(i).getIdProduit(),obj.get(i));
        }

        ArrayList<Article> tmp = new ArrayList<>();
        for (Article article: singleFav.articlesFavoris) {
            Article favori = singleArt.articlesDico.get(article.getIdProduit());
            favori.inFavoris = true;
            tmp.add(favori);
        }

        singleFav.articlesFavoris = tmp;
        singleArt.articles = obj;
        makeGrid(obj);
    }
}