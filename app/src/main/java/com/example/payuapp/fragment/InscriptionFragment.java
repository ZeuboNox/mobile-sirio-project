package com.example.payuapp.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.payuapp.API.RequestAPI;
import com.example.payuapp.API.HandlerAPI;
import com.example.payuapp.ArianneForFragment;
import com.example.payuapp.Parameter;
import com.example.payuapp.R;
import com.example.payuapp.Singleton.Singleton;
import com.example.payuapp.Singleton.SingletonWaiting;
import com.example.payuapp.commande.handlerCommande;
import com.example.payuapp.connexion.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;

/**
 * formulaire d'inscription
 *
 * A simple {@link Fragment} subclass.
 * Use the {@link InscriptionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class InscriptionFragment extends Fragment implements HandlerAPI<ResponseBody>{

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    final ArianneForFragment arianne = (ArianneForFragment) Singleton.getInstance(ArianneForFragment.class);
    final SingletonWaiting waiter = (SingletonWaiting) Singleton.getInstance(SingletonWaiting.class);
    final User user = (User) Singleton.getInstance(User.class);

    EditText name;
    EditText email;
    EditText pass;
    EditText repass;
    TextView backConnexion;
    Button button;

    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =Pattern.compile("^[a-z0-9._%+-]*@[^-][A-Za-z0-9-]+(\\\\.[A-Za-z0-9-]+)*(\\\\.[A-Za-z]{2,})$", Pattern.CASE_INSENSITIVE);

    public InscriptionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment InscriptionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static InscriptionFragment newInstance(String param1, String param2) {
        InscriptionFragment fragment = new InscriptionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_inscription, container, false);
        name = view.findViewById(R.id.inscription_name);
        email = view.findViewById(R.id.inscription_email);
        pass = view.findViewById(R.id.inscription_password);
        repass = view.findViewById(R.id.inscription_repass);
        button = view.findViewById(R.id.inscription_button);
        backConnexion = view.findViewById(R.id.back_connexion);

        makeButton();
        makeBackConnexion();

        return view;
    }

    /**
     * verifie la validité de l'email
     * @return :boolean
     */
    private boolean isEmailValid() {
        Pattern pattern = VALID_EMAIL_ADDRESS_REGEX;
        Matcher matcher = pattern.matcher(email.getText().toString());
        return matcher.matches();
    }

    /**
     * verifie la coerence des mots de passes entrés
     * @return :boolean
     */
    private boolean isSamePassword(){
        return pass.getText() == repass.getText();
    }

    /**
     * methode qui instancie le boutton pour effectuer l'inscription
     */
    private void makeButton(){
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isEmailValid()){
                    Toast.makeText(getContext(),"le format de l'email est erroné",Toast.LENGTH_SHORT).show();
                    return;
                }
                if(isSamePassword()){
                    Toast.makeText(getContext(),"les mot de passe sont différent",Toast.LENGTH_SHORT).show();
                    return;
                }
                waiter.progressDialog = ProgressDialog.show(getContext(),"Connexion", "patientez...",false,false);

                Parameter params = new Parameter();
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("email", email.getText().toString());
                    jsonObject.put("name", name.getText().toString());
                    jsonObject.put("password", pass.getText().toString());
                    jsonObject.put("createdAt", null);
                    jsonObject.put("updateAt", null);
                    jsonObject.put("role", "user");
                    jsonObject.put("root", false);
                    jsonObject.put("avatar", "");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                params.request = RequestBody.create(MediaType.parse("application/json"), "["+jsonObject.toString()+"]");

                RequestAPI<ResponseBody> api = new RequestAPI<>();
                api.Api(InscriptionFragment.this,"PostUser", params);
            }
        });
    }

    /**
     * methode qui instancie le bouton pour revenir au formulaire de connexion
     */
    private void makeBackConnexion(){
        backConnexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arianne.nextPage(new ConnexionFragment());
            }
        });
    }

    @Override
    public void Awake(ResponseBody obj) {
        RequestAPI<User> api = new RequestAPI<User>();
        Parameter parms = new Parameter();
        parms.user = email.getText().toString();
        parms.pass = pass.getText().toString();
        api.Api(new HandlerAPI<User>() {
            @Override
            public void Awake(User obj) {
                waiter.progressDialog.dismiss();
                if (obj != null) {
                    user.toLoad(obj);
                    new handlerCommande(obj.getId());
                    Singleton.save(getContext(), User.class);
                    Toast.makeText(getContext(),"Bienvenue parmi nous", Toast.LENGTH_SHORT).show();
                    arianne.startPage(new OptionUserFragment());
                };
            }
        }, "User", parms);
    }
}