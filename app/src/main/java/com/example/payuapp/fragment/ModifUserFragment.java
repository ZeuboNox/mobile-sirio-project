package com.example.payuapp.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.payuapp.API.RequestAPI;
import com.example.payuapp.API.HandlerAPI;
import com.example.payuapp.ArianneForFragment;
import com.example.payuapp.Parameter;
import com.example.payuapp.R;
import com.example.payuapp.Singleton.Singleton;
import com.example.payuapp.connexion.User;

import okhttp3.ResponseBody;

/**
 * Permet de modifier les information de l'utilisateur
 *
 * A simple {@link Fragment} subclass.
 * Use the {@link ModifUserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ModifUserFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    final ArianneForFragment arianne = (ArianneForFragment) Singleton.getInstance(ArianneForFragment.class);
    final User user = (User) Singleton.getInstance(User.class);

    private TextView nom;
    private TextView prenom;
    private TextView email;
    private TextView numero;
    private TextView adresse;
    private TextView codePostal;
    private TextView ville;
    private TextView pays;

    private Button deconnexion;
    private Button valider;

    public ModifUserFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ModifUserFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ModifUserFragment newInstance(String param1, String param2) {
        ModifUserFragment fragment = new ModifUserFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_modif_user, container, false);
        init(view);
        nom.setText(user.getNom());
        prenom.setText(user.getPrenom());
        email.setText(user.getEmail());
        numero.setText(user.getTelephone());
        adresse.setText(user.getAdresse());
        codePostal.setText(user.getCodePostal());
        ville.setText(user.getVille());
        pays.setText(user.getPaysRegion());

        settingValider();
        return view;
    }

    private void init(View view){
        nom = view.findViewById(R.id.nom_user_edit);
        prenom = view.findViewById(R.id.prenom_user_edit);
        email = view.findViewById(R.id.email_user_edit);
        numero = view.findViewById(R.id.numero_user_edit);
        adresse = view.findViewById(R.id.adresse_user_edit);
        codePostal = view.findViewById(R.id.postal_user_edit);
        ville = view.findViewById(R.id.ville_user_edit);
        pays = view.findViewById(R.id.pays_user_edit);

        valider = view.findViewById(R.id.button_valider);

    }

    /**
     * methode qui instancie le bouton pour sauvegarder les nouvelles données de l'utilsateur et renvoie vers UserFragment
     */
    private void settingValider(){
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user.setNom(nom.getText().toString());
                user.setPrenom(prenom.getText().toString());
                user.setEmail(email.getText().toString());
                user.setTelephone(numero.getText().toString());
                user.setAdresse(adresse.getText().toString());
                user.setCodePostal(codePostal.getText().toString());
                user.setVille(ville.getText().toString());
                user.setPaysRegion(pays.getText().toString());

                RequestAPI<ResponseBody> api = new RequestAPI<>();
                Parameter params = new Parameter();
                params.objUser = user;
                api.Api(new HandlerAPI<ResponseBody>() {
                    @Override
                    public void Awake(ResponseBody obj) {

                    }
                },"UpdateUser", params);
                Singleton.save(getContext(), User.class);

                arianne.nextPage(new UserFragment());
            }
        });
    }
}