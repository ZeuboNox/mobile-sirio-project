package com.example.payuapp.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.payuapp.ArianneForFragment;
import com.example.payuapp.R;
import com.example.payuapp.Singleton.Singleton;
import com.example.payuapp.commande.handlerCommande;
import com.example.payuapp.connexion.User;

/**
 * affiche les bouton qui concerne le compte de l'utilisateur
 *
 * A simple {@link Fragment} subclass.
 * Use the {@link OptionUserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class OptionUserFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    final ArianneForFragment arianne = (ArianneForFragment) Singleton.getInstance(ArianneForFragment.class);
    final User user = (User) Singleton.getInstance(User.class);

    private Button buttonInformation;
    private Button buttonCommande;
    private Button buttonDeconnexion;
    private Button buttonPayment;

    public OptionUserFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment OptionUserFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static OptionUserFragment newInstance(String param1, String param2) {
        OptionUserFragment fragment = new OptionUserFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_option_user, container, false);
        buttonInformation = view.findViewById(R.id.option_information);
        buttonDeconnexion = view.findViewById(R.id.option_deconnexion);
        buttonCommande = view.findViewById(R.id.option_commande);
        new handlerCommande(user.getId());

        makeButtonInformation();
        makeButtonDeconnexion();
        makeButtonCommande();

        return view;
    }

    public void makeButtonInformation() {
        buttonInformation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arianne.nextPage(new UserFragment());
            }
        });
    }

    public void makeButtonDeconnexion(){
        buttonDeconnexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Singleton.deleteSave(getContext(), User.class);
                arianne.nextPage(new PortailFragment());
            }
        });
    }
    public void makeButtonCommande(){
        buttonCommande.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arianne.nextPage(new CommandeFragment());
            }
        });
    }
}