package com.example.payuapp.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.payuapp.ArianneForFragment;
import com.example.payuapp.Article.Article;
import com.example.payuapp.BackButton;
import com.example.payuapp.R;
import com.example.payuapp.Singleton.Singleton;
import com.example.payuapp.commande.Cart;
import com.example.payuapp.panier.Panier;
import com.example.payuapp.panier.PanierAdaptater;

import java.util.List;

/**
 * affiche la liste des articles dans le panier
 *
 * A simple {@link Fragment} subclass.
 * Use the {@link PanierFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PanierFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    final ArianneForFragment arianne = (ArianneForFragment) Singleton.getInstance(ArianneForFragment.class);
    final Panier panier = (Panier) Singleton.getInstance(Panier.class);

    ListView listView;
    TextView total;
    Button buttonPayment;
    float value;

    public PanierFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PanierFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PanierFragment newInstance(String param1, String param2) {
        PanierFragment fragment = new PanierFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        value = 0;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_panier, container, false);
        listView = view.findViewById(R.id.listviewPanier);
        total = view.findViewById(R.id.panier_total);
        buttonPayment = view.findViewById(R.id.paymentPanier);

        makeList(panier.getArticles());
        makeVider();
        makePayment();

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Button buttonInteract = getActivity().findViewById(R.id.buttonInteract);
        buttonInteract.setEnabled(true);
        BackButton.disable(buttonInteract);
    }

    private void makeList(List<Article> articles){
        PanierAdaptater customAdapter = new PanierAdaptater(getContext(), articles, this);
        listView.setAdapter(customAdapter);

    }

    private void makePayment(){
        buttonPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("DEBUGAGE", "why " +PanierAdaptater.prods.get(0).getQuantity()+"");
                arianne.nextPage(new CheckFragment(value, PanierAdaptater.prods.toArray(new Cart[PanierAdaptater.prods.size()])));
            }
        });
    }

    private void makeVider(){
        Button buttonInteract = getActivity().findViewById(R.id.buttonInteract);
        BackButton.active(buttonInteract);
        buttonInteract.setText("VIDER PANIER");
        buttonInteract.setTextSize(20);
        if(panier.getArticles().size() == 0){
            buttonInteract.setEnabled(false);
        }
        buttonInteract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                panier.clear();
                makeList(panier.getArticles());
                total.setText("0");
                buttonInteract.setEnabled(false);
            }
        });
    }

    public void awake(float value){
        total.setText(String.format("%.2f",value)+" €");
        this.value = value;
        Log.d("DEBUGAGE",String.valueOf(value));
    }
}