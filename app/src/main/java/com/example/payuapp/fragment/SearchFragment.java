package com.example.payuapp.fragment;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;

import com.example.payuapp.ArianneForFragment;
import com.example.payuapp.Article.Article;
import com.example.payuapp.R;
import com.example.payuapp.Search.SearchAdapter;
import com.example.payuapp.Search.TreeSearch;
import com.example.payuapp.Singleton.Singleton;
import com.example.payuapp.Singleton.SingletonArticles;
import com.example.payuapp.Singleton.SingletonTreeSearch;
import com.example.payuapp.sort.SortByWeight;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * affiche le moteur de recherche
 *
 * A simple {@link Fragment} subclass.
 * Use the {@link SearchFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SearchFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    final ArianneForFragment arianne = (ArianneForFragment) Singleton.getInstance(ArianneForFragment.class);
    final SingletonTreeSearch singleTree = (SingletonTreeSearch) Singleton.getInstance(SingletonTreeSearch.class);
    final SingletonArticles singleArt = (SingletonArticles) Singleton.getInstance(SingletonArticles.class);

    private EditText searchText;
    private GridView gridViewt;

    public SearchFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SearchFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SearchFragment newInstance(String param1, String param2) {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);
        searchText = view.findViewById(R.id.searchText);
        gridViewt = view.findViewById(R.id.searchGridView);

        setSearchEditText(view, singleTree.tree);
        setSendEditText(view);
        return view;
    }

    public void setSearchEditText(View view, TreeSearch tr){
        searchText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ArrayList<String> listConcatWord = new ArrayList<>();
                String[] words = s.toString().split(" ");
                for(String word : words){
                    listConcatWord = (ArrayList<String>) Stream.concat(listConcatWord.stream(), tr.search(word).stream()).collect(Collectors.toList());
                }
                setSearchGrid(view, listConcatWord);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void setSearchGrid(View view, ArrayList<String> result){
        SearchAdapter adapter = new SearchAdapter(getContext(), result);
        Log.d("DEBUGAGE", result.toString());
        gridViewt.setAdapter(adapter);
        gridViewt.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onItemClick(AdapterView<?> parent, View v, int i, long id) {
                TextView search = v.findViewById(R.id.findTextLayout);
                //ArrayList<Article> result = searching(search.getText().toString());
                String[] words = searchText.getText().toString().split(" ");
                words[words.length-1] = search.getText().toString();
                searchText.setText(String.join(" ", words));
            }
        });
    }

    public ArrayList<Article> searching(String txtSearch){
        ArrayList<Article> foundArticle = new ArrayList<Article>();
        List<Article> allArticle = singleArt.articles;
        for (Article art: allArticle) {
            if(art.getTreeTag().search(txtSearch).size() > 0){
                art.setPriceWeight(art.getPriceWeight()+1);
                foundArticle.add(art);
            }
        }
        return foundArticle;
    }

    public void setSendEditText(View view){
        TextView.OnEditorActionListener editor = new TextView.OnEditorActionListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch(actionId){
                    case EditorInfo.IME_ACTION_SEND:
                        String[] words = v.getText().toString().split(" ");
                        ArrayList<Article> result = new ArrayList<>();
                        for(String word: words) {
                            result = (ArrayList<Article>) Stream.concat(result.stream(), searching(word).stream()).distinct().collect(Collectors.toList());
                        }
                        Collections.sort(result, new SortByWeight());
                        for (Article art: result) { art.setPriceWeight(0);}
                        arianne.nextPage(new HomeFragment(result));
                        break;
                }
                return false;
            }
        };

        searchText.setOnEditorActionListener(editor);

    }


}