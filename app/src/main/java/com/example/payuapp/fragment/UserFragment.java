package com.example.payuapp.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.payuapp.ArianneForFragment;
import com.example.payuapp.R;
import com.example.payuapp.Singleton.Singleton;
import com.example.payuapp.connexion.User;

/**
 * affiche les information de l'utilisateur
 *
 * A simple {@link Fragment} subclass.
 * Use the {@link UserFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UserFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    final ArianneForFragment arianne = (ArianneForFragment) Singleton.getInstance(ArianneForFragment.class);
    final User user = (User) Singleton.getInstance(User.class);

    private TextView nom;
    private TextView prenom;
    private TextView email;
    private TextView numero;
    private TextView adresse;
    private TextView codePostal;
    private TextView ville;
    private TextView pays;

    private Button deconnexion;
    private Button modifier;

    public UserFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UserFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UserFragment newInstance(String param1, String param2) {
        UserFragment fragment = new UserFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(user == null){
            arianne.nextPage(new ConnexionFragment());
            return;
        }

        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user, container, false);

        init(view);
        settingModifier();

        if(user.getNom() != "")nom.setText(user.getNom());
        if(user.getPrenom() != "")prenom.setText(user.getPrenom());
        if(user.getEmail() != "")email.setText(user.getEmail());
        if(user.getTelephone() != "")numero.setText(user.getTelephone());
        if(user.getAdresse() != "")adresse.setText(user.getAdresse());
        if(user.getCodePostal() != "")codePostal.setText(user.getCodePostal());
        if(user.getVille() != "")ville.setText(user.getVille());
        if(user.getPaysRegion() != "")pays.setText(user.getPaysRegion());

        return view;
    }

    private void init(View view){
        nom = view.findViewById(R.id.nom_user_txt);
        prenom = view.findViewById(R.id.prenom_user_txt);
        email = view.findViewById(R.id.email_user_txt);
        numero = view.findViewById(R.id.numero_user_txt);
        adresse = view.findViewById(R.id.adresse_user_txt);
        codePostal = view.findViewById(R.id.postal_user_txt);
        ville = view.findViewById(R.id.ville_user_txt);
        pays = view.findViewById(R.id.pays_user_txt);

        modifier = view.findViewById(R.id.button_modifier);

    }


    private void settingModifier(){
        modifier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arianne.nextPage(new ModifUserFragment());
            }
        });
    }
}