package com.example.payuapp.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.payuapp.API.HandlerAPI;
import com.example.payuapp.JavaMailAPI;
import com.example.payuapp.R;
import com.example.payuapp.Singleton.Singleton;
import com.example.payuapp.commande.Cart;
import com.example.payuapp.commande.Commande;
import com.example.payuapp.commande.handlerCommande;
import com.example.payuapp.connexion.User;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * lorsque le payment c'est bien passé, affiche le recapitulatif d'une commande
 *
 * A simple {@link Fragment} subclass.
 * Use the {@link ValidPaymentFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ValidPaymentFragment extends Fragment{

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    final User user = (User) Singleton.getInstance(User.class);

    Commande commande;

    public ValidPaymentFragment() {
        // Required empty public constructor
    }
    public ValidPaymentFragment(Commande commande) {
        this.commande = commande;
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ValidPaymentFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ValidPaymentFragment newInstance(String param1, String param2) {
        ValidPaymentFragment fragment = new ValidPaymentFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        String strArt ="";
        int i=1;
        for(Cart c: commande.getCart()){
            strArt += "produit " + i + " : "+ c.getId() +"\n" +
                    "\t"+ c.getTitle() + "\n" +
                    "\tprix : "+ c.getPrice() +"\n" +
                    "\tquantité : "+ c.getQuantity() +"\n\n";
            i++;
        }

        String message = "Confirmation de commande\n" +
                "Commande n° " + commande.getPaymentId() +
                " effectué le " + new SimpleDateFormat("dd/MM/YYYY").format(new Date()) +
                "\n\n\nBonjour, \n" +
                "Nous vous remercions de votre commande. Nous vous tiendrons informé par e-mail lorsque les articles de votre commande auront été expédiés." +
                " Vous pouvez suivre l’état de votre commande sur le site ou l'application SIRIO \n" +
                "\n" + strArt +
                "total: " + String.format("%.2f",commande.getTotal()) +" €"+
                "\n\nNous espérons vous revoir bientôt";
        JavaMailAPI mail = new JavaMailAPI(getContext(), user.getEmail(), "Votre Commande SIRIO", message);
        mail.execute();
        View view = inflater.inflate(R.layout.fragment_valid_payment, container, false);
        handlerCommande handle = new handlerCommande();
        TextView textView = view.findViewById(R.id.valide_commande);
        String text = "Votre Commande " + commande.getPaymentId() + " a été enregistré\n" +
                "nous vous remercier d'avoir fait confiance a SIRIO";
        textView.setText(text);
        return view;
    }
}