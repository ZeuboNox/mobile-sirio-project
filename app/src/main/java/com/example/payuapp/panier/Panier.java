package com.example.payuapp.panier;

import com.example.payuapp.Article.Article;
import com.example.payuapp.Singleton.Singleton;
import com.google.android.material.badge.BadgeDrawable;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * classe qui permet de gerer le panier  avec une liste d'articles associé
 */
public class Panier extends Singleton {

    ArrayList<Article> articles;
    BadgeDrawable badge;
    int lenght;

    public Panier(){
        articles = new ArrayList<>();
        lenght = 0;
    }

    public void setBadge(BadgeDrawable badge){
        this.badge = badge;
    }

    public void add(Article article){
        articles.add(article);
        lenght+=1;
        badge.setVisible(true);
        badge.setNumber(lenght);
    }

    public void remove(Article article){
        articles.remove(article);
        lenght-=1;
        if(lenght == 0){
            badge.setVisible(false);
        }
        badge.setNumber(lenght);
    }

    public List<Article> getArticles(){
        return articles;
    }

    public void setArticles(List<Article> articles){
        this.articles = (ArrayList<Article>) articles;
    }

    public Article get(int i){
        return articles.get(i);
    }

    public void clear(){
        for (Article art:articles) {
            art.inPanier = false;
        }
        lenght = 0;
        badge.setVisible(false);
        articles.clear();
    }

    @Override
    public Object toSave() {
        return null;
    }

    @Override
    public void reset() {

    }

    @Override
    public Type type() {
        return null;
    }

    @Override
    public void toLoad(Object obj) {

    }
}
