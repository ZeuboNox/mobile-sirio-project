package com.example.payuapp.panier;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.payuapp.ArianneForFragment;
import com.example.payuapp.Article.Article;
import com.example.payuapp.R;
import com.example.payuapp.Singleton.Singleton;
import com.example.payuapp.commande.Cart;
import com.example.payuapp.fragment.ArticleFragment;
import com.example.payuapp.fragment.PanierFragment;
import com.zozx.glider.Glider;

import java.util.ArrayList;
import java.util.List;

/**
 * Adapter pour afficher la liste des articles dans la listView du fragment PanierFragment
 */
public class PanierAdaptater extends BaseAdapter {

    Context context;
    List<Article> articles;
    LayoutInflater inflater;
    PanierFragment panier;
    public static ArrayList<Cart> prods;

    final ArianneForFragment arianne = (ArianneForFragment) Singleton.getInstance(ArianneForFragment.class);

    int[] number;

    public PanierAdaptater(Context context, List<Article> articles, PanierFragment panier){
        number = new int[articles.size()];
        for(int i=0; i<number.length;i++){
            number[i] = 1;
        }
        this.context = context;
        this.articles = articles;
        inflater = LayoutInflater.from(context);
        this.panier = panier;
        prods = new ArrayList<>();
        makeProds();
    }

    void makeProds(){
        for(int i =0; i<articles.size();i++){
            prods.add(articles.get(i).toCart());
        }
    }

    @Override
    public int getCount() {
        return articles.size();
    }

    @Override
    public Object getItem(int position) {
        return articles.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.panier_layout, null); // inflate the layout
        TextView title = convertView.findViewById(R.id.panier_title);
        TextView desc = convertView.findViewById(R.id.panier_description);
        TextView price = convertView.findViewById(R.id.panier_price);
        ImageView img = convertView.findViewById(R.id.panier_image);
        ImageButton top = convertView.findViewById(R.id.panier_increase);
        ImageButton bot = convertView.findViewById(R.id.panier_decrease);
        TextView txtNum = convertView.findViewById(R.id.panier_number);
        LinearLayout onclick = convertView.findViewById(R.id.panier_onclick);

        Glider.load(img, articles.get(i).getImages().get(0).getUrl());

        String lab = articles.get(i).getLibelle();
        String des = articles.get(i).getDescription();
        title.setText(lab.length()>=27?lab.substring(0,27)+"...":lab);
        desc.setText(des.length()>=47?des.substring(0,47)+"...":lab);
        price.setText(articles.get(i).getPrix().toString() + "€");

        panier.awake(getTotalPrice());

        onclick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                arianne.nextPage(new ArticleFragment(articles.get(i)));
            }
        });

        top.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(articles.get(i).getNbStock() >= number[i]+1){
                    number[i] += 1;
                    txtNum.setText(String.valueOf(number[i]));
                    panier.awake(getTotalPrice());
                    prods.get(i).setQuantity(number[i]);
                    Log.d("DEBUGAGE","number = "+number[i]+ " with get " +prods.get(i).getQuantity());
                }
            }
        });
        bot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(number[i] > 1){
                    number[i] -= 1;
                    txtNum.setText(String.valueOf(number[i]));
                    panier.awake(getTotalPrice());
                    prods.get(i).setQuantity(prods.get(i).getQuantity()-1);
                }
            }
        });


        return convertView;
    }
    
    public float getTotalPrice(){
        float somme = 0;
        for (int i =0; i < number.length;i++) {
            somme += Float.parseFloat(articles.get(i).getPrix()) * number[i];
        }
        return somme;
    }


}
