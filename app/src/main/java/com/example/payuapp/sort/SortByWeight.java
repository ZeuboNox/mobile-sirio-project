package com.example.payuapp.sort;

import com.example.payuapp.Article.Article;

import java.util.Comparator;

public class SortByWeight implements Comparator<Article> {

    @Override
    public int compare(Article o1, Article o2) {
        Integer a = o1.getPriceWeight();
        Integer b = o2.getPriceWeight();
        return b.compareTo(a);
    }
}
